using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NoticeApi.Models;

namespace NoticeApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NoticeItemsController : ControllerBase
    {
        private readonly NoticeContext _context;

        public NoticeItemsController(NoticeContext context)
        {
            _context = context;
        }

        /// <summary>
        ///  Get all notices
        /// </summary>
        // GET: api/NoticeItems
        [HttpGet]
        public async Task<ActionResult<IEnumerable<NoticeItem>>> GetNoticeItems()
        {
            return await _context.NoticeItems.ToListAsync();
        }

        /// <summary>
        ///  Get a notice by id
        /// </summary>
        // GET: api/NoticeItems/5
        [HttpGet("{id}")]
        public async Task<ActionResult<NoticeItem>> GetNoticeItem(long id)
        {
            var noticeItem = await _context.NoticeItems.FindAsync(id);

            if (noticeItem == null)
            {
                return NotFound();
            }

            return noticeItem;
        }

        /// <summary>
        ///  Edit notice by id
        /// </summary>
        // PUT: api/NoticeItems/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutNoticeItem(long id, NoticeItem noticeItem)
        {
            if (id != noticeItem.Id)
            {
                return BadRequest();
            }

            _context.Entry(noticeItem).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!NoticeItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        ///  Create notice in "InMemoryDatabase"
        /// </summary>
        // POST: api/NoticeItems
        [HttpPost]
        public async Task<ActionResult<NoticeItem>> PostNoticeItem(NoticeItem noticeItem)
        {
            _context.NoticeItems.Add(noticeItem);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetNoticeItem), new { id = noticeItem.Id }, noticeItem);
        }

        /// <summary>
        ///  Delete notice by id
        /// </summary>
        // DELETE: api/NoticeItems/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<NoticeItem>> DeleteNoticeItem(long id)
        {
            var noticeItem = await _context.NoticeItems.FindAsync(id);
            if (noticeItem == null)
            {
                return NotFound();
            }

            _context.NoticeItems.Remove(noticeItem);
            await _context.SaveChangesAsync();

            return noticeItem;
        }

        private bool NoticeItemExists(long id)
        {
            return _context.NoticeItems.Any(e => e.Id == id);
        }
    }
}
