using Microsoft.EntityFrameworkCore;

namespace NoticeApi.Models
{
    public class NoticeContext : DbContext
    {
        public NoticeContext(DbContextOptions<NoticeContext> options)
            : base(options)
        {
        }

        public DbSet<NoticeItem> NoticeItems { get; set; }
    }
}