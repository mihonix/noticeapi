using System;
using System.ComponentModel.DataAnnotations;

namespace NoticeApi.Models
{
    public class NoticeItem
    {
        public long Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Text { get; set; }
        [Required]
        public int Priority { get; set; }
        [Required]
        public DateTime Date { get; set; }
    }
}
