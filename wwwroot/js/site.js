const uri = 'api/NoticeItems';
let notices = [];

function getNotices() {
  fetch(uri)
    .then(response => response.json())
    .then(data => _displayNotices(data))
    .catch(error => console.error('Unable to get notices.', error));
}

function getTaskboard() {
  fetch(uri)
    .then(response => response.json())
    .then(data => fillTaskboard(data))
    .catch(error => console.error('Unable to get taskboard.', error));
}

function addNotice() {
  const addNameTextbox = document.getElementById('add-name');
  const addTextTextbox = document.getElementById('add-text');
  const addDateTextbox = document.getElementById('add-date');
  const addPriorityTextbox = document.getElementById('add-priority');

  const notice = {
    name: addNameTextbox.value.trim(),
    text: addTextTextbox.value.trim(),
    date: addDateTextbox.value.trim(),
    priority: addPriorityTextbox.value.trim()  
  };


  fetch(uri, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(notice)
  })
    .then(response => response.json())
    .then(() => {
      getNotices();
      addNameTextbox.value = '';
      addTextTextbox.value = '';
      addDateTextbox.valueAsDate = new Date();
      addPriorityTextbox.value = '3';
    })
    .catch(error => console.error('Unable to add notice.', error));
}


function deleteNotice(id) {
  fetch(`${uri}/${id}`, {
    method: 'DELETE'
  })
  .then(() => getNotices())
  .catch(error => console.error('Unable to delete notice.', error));
}

function displayEditForm(id) {
  const notice = notices.find(notice => notice.id === id);
  
  document.getElementById('edit-name').value = notice.name;
  document.getElementById('edit-text').value = notice.text;
  document.getElementById('edit-date').value = notice.date.slice(0,10);
  document.getElementById('edit-priority').value = notice.priority;
  document.getElementById('edit-id').value = notice.id;
  document.getElementById('editForm').style.display = 'block';
}

function updateNotice() {
  const noticeId = document.getElementById('edit-id').value;
  const notice = {
    id: parseInt(noticeId, 10),
    name: document.getElementById('edit-name').value.trim(),
    text: document.getElementById('edit-text').value.trim(),
    date: document.getElementById('edit-date').value.trim(),
    priority: document.getElementById('edit-priority').value.trim()
  };

  fetch(`${uri}/${noticeId}`, {
    method: 'PUT',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(notice)
  })
  .then(() => getNotices())
  .catch(error => console.error('Unable to update notice.', error));

  closeInput();

  return false;
}

function closeInput() {
  document.getElementById('editForm').style.display = 'none';
}

function _displayCount(noticeCount) {
  const name = (noticeCount === 1) ? 'Notiz' : 'Notizen';

  document.getElementById('counter').innerText = `${noticeCount} ${name}`;
}

function displayNoticeList() {
  closeInput();
  $('#noticeList').removeClass('d-none');
}

function _displayNotices(data) {
  if(data.length>0) displayNoticeList();

  const tBody = document.getElementById('notices');
  tBody.innerHTML = '';

  _displayCount(data.length);

  const button = document.createElement('button');

  data.forEach(notice => {
    let editButton = button.cloneNode(false);
    editButton.innerText = 'Ändern';
    editButton.setAttribute('class','btn btn-secondary btn-margin');
    editButton.setAttribute('onclick', `displayEditForm(${notice.id})`);

    let deleteButton = button.cloneNode(false);
    deleteButton.innerText = 'Löschen';
    deleteButton.setAttribute('class','btn btn-secondary');
    deleteButton.setAttribute('onclick', `deleteNotice(${notice.id})`);

    let tr = tBody.insertRow();
      
    let td1 = tr.insertCell(0);
    let textNode = document.createTextNode(notice.name);
    td1.appendChild(textNode);
    
    let td2 = tr.insertCell(1);
    let text1Node = document.createTextNode(notice.text);
    td2.appendChild(text1Node);

    let td3 = tr.insertCell(2);
    let dateObj = new Date(Date.parse(notice.date));
    let text2Node = document.createTextNode(dateObj.toLocaleDateString());
    td3.appendChild(text2Node);

    let td4 = tr.insertCell(3);
    let textNodeText = document.createTextNode(document.getElementById("add-priority").options[notice.priority-1].text);
    td4.appendChild(textNodeText);

    let td5 = tr.insertCell(4);
    td5.colSpan = 2
    td5.appendChild(editButton);
    td5.appendChild(deleteButton);
  });

  notices = data;
  
}

function createDiv(classname) 
{
  div = document.createElement('div');
  div.setAttribute('class',classname);
  return div;
}

function createPostit(row,priority) 
{
  var col = row.appendChild(createDiv('col').appendChild(createDiv('postit')));
  switch(priority) {
    case 1:
      col.setAttribute('style','background:red !important');
      break;
    case 2:
      col.setAttribute('style','background:green !important');
      break;
  } 
  return col;
}

function fillTaskboard(data) 
{
    const taskboard = document.getElementById('taskboard'); 
    var i = 0;
    var row = taskboard.appendChild(createDiv('row')); 
    data.forEach(notice => {
       if(i%4 == 0) {
          row = taskboard.appendChild(createDiv('row'));    
       }
       postit = row.appendChild(createPostit(row,notice.priority));
       headertg = document.createElement('h2');
       headerstr = document.createTextNode(notice.name);
       datetg = document.createElement('p');
       dateobj = new Date(Date.parse(notice.date));
       datestr = document.createTextNode(dateobj.toLocaleDateString());
       texttg = document.createElement('p');
       textstr = document.createTextNode(notice.text);
       postit.appendChild(headertg).appendChild(headerstr);
       postit.appendChild(datetg).appendChild(datestr);
       postit.appendChild(texttg).appendChild(textstr);
       i++;
    });

    notices = data;
}
